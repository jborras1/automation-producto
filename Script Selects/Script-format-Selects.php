<?php
function fillContent($labelName, $internalValue, $arrayPosition){
    $contentFile = '
    <div class="sortable">
    <div class="input-group sortable-no-reorder">
        <span class="input-group-addon preaddon" onclick="Mautic.removeFormListOption(this);">
            <i class="fa fa-times"></i>
        </span>
        <div>
            <div class="row">
                <div class="col-xs-6 mr-0 pr-0 bdr-r-wdh-0">
                    <input type="text" id="leadfield_properties_list_' . $arrayPosition . '_label"
                        name="leadfield[properties][list][' . $arrayPosition . '][label]"
                        class="form-control sortable-label" placeholder="Etiqueta" autocomplete="false"
                        value="' . $labelName . '">
                </div>
                <div class="col-xs-6 ml-0 pl-0">
                    <input type="text" id="leadfield_properties_list_' . $arrayPosition . '_value"
                        name="leadfield[properties][list][' . $arrayPosition . '][value]"
                        class="form-control sortable-value" placeholder="Valor" autocomplete="false"
                        value="' . $internalValue . '">
                </div>
            </div>
        </div>
        <span class="input-group-addon postaddon ui-sortable-handle">
            <i class="fa fa-ellipsis-v handle"></i>
        </span>
    </div>
    </div>';

    return $contentFile;
}

$filename = "values-selects-mautic.txt";
$formatedFileTop = '<div id="sortable-leadfield_properties" class="list-sortable ui-sortable">';
$formatedFileBottom = '</div>';
$contentFormated = '';

$arraySelect = [];
$file = fopen( $filename, "r" );
$line = fgets($file);

while (($line = fgets($file)) !== false) {
    $line = str_replace("\r\n", "", $line);
    $arrayValues = explode(',', $line);
    $arraySelect[$arrayValues[1]] = $arrayValues[0];
}
fclose($file);

$position = 0;
foreach($arraySelect as $keyRowSelect => $valueRowSelect){
    $contentFormated .= fillContent($valueRowSelect, $keyRowSelect, $position);
    $position++;
}

$myfile = fopen("selects-formateados.txt", "w") or die("No se ha podido abrir el archivo. Contactad con Producto.");
$txt = $formatedFileTop . $contentFormated . $formatedFileBottom;
fwrite($myfile, $txt);
fclose($myfile);